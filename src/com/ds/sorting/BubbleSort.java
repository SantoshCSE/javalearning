package com.ds.sorting;


public class BubbleSort {

    public static void main(String[] args) {
        int unsortedIntArray[] = {2, 22, 11, 99, 66, 55, 33, 77, 11, -1, -2, 111};
        String unsortedStringArray[] = {"Santosh", "Alok", "Vinod", "Ashok", "Ganga", "Rajni"};
        bubbleSortForIntArray(unsortedIntArray);
        System.out.println();
        bubbleSortForStringArray(unsortedStringArray);
    }

    /**
     * Bubble Sort for Int Array
     *
     * @param unsortedArray
     */
    private static void bubbleSortForIntArray(int[] unsortedArray) {
        int lengthOfArray = unsortedArray.length;
        int temp;
        boolean flag = false;
        // for number of iteration required
        for (int i = 0; i < lengthOfArray; i++) {
            // for comparison and -i to remove comparison in already sorted
            for (int j = 0; j < lengthOfArray - 1 - i; j++) {
                if (unsortedArray[j] > unsortedArray[j + 1]) {
                    temp = unsortedArray[j];
                    unsortedArray[j] = unsortedArray[j + 1];
                    unsortedArray[j + 1] = temp;
                    flag = true;
                }
            }
            // If array is already sorted so no need to check again & again
            if (!flag) {
                break;
            }
        }

        // Printing sorted array
        for (int i = 0; i < unsortedArray.length; i++) {
            System.out.print(unsortedArray[i] + " ");
        }
    }

    /**
     * Bubble Sort for String Array
     *
     * @param unsortedArray
     */
    private static void bubbleSortForStringArray(String[] unsortedArray) {
        int lengthOfArray = unsortedArray.length;
        String temp;
        boolean flag = false;
        // for number of iteration required
        for (int i = 0; i < lengthOfArray; i++) {
            // for comparison and -i to remove comparison in already sorted
            for (int j = 0; j < lengthOfArray - 1 - i; j++) {
                if (unsortedArray[j].compareTo(unsortedArray[j + 1]) > 0) {
                    temp = unsortedArray[j];
                    unsortedArray[j] = unsortedArray[j + 1];
                    unsortedArray[j + 1] = temp;
                    flag = true;
                }
            }
            // If array is already sorted so no need to check again & again
            if (!flag) {
                break;
            }
        }

        // Printing sorted array
        for (int i = 0; i < unsortedArray.length; i++) {
            System.out.print(unsortedArray[i] + " ");
        }
    }

}
