package com.ds.sorting;

public class SelectionSort {

    public static void main(String[] args) {
        int unsortedIntArray[] = {2, 22, 11, 99, 66, 55, 33, 77, 11, -1, -2, 111};
        String unsortedStringArray[] = {"Santosh", "Alok", "Vinod", "Ashok", "Ganga", "Rajni"};
        selectionSortForIntArray(unsortedIntArray);
        System.out.println();
        selectionSortForStringArray(unsortedStringArray);
    }
    /**
     * Method to sort integer number
     *
     * @param arrayNeedToSort
     */
    private static void selectionSortForIntArray(int[] arrayNeedToSort) {
        int temp = 0;
        int min = 0;
        for (int i = 0; i < arrayNeedToSort.length; i++) {
            for (int j = i + 1; j < arrayNeedToSort.length; j++) {
                if (arrayNeedToSort[j] < arrayNeedToSort[min]) {
                    min = j;
                }
            }

            temp = arrayNeedToSort[i];
            arrayNeedToSort[i] = arrayNeedToSort[min];
            arrayNeedToSort[min] = temp;
        }

        // Printing sorted array
        for (int i = 0; i < arrayNeedToSort.length; i++) {
            System.out.print(arrayNeedToSort[i] + " ");
        }
    }

    /**
     * Method to sort String
     *
     * @param arrayNeedToSort
     */
    private static void selectionSortForStringArray(String[] arrayNeedToSort) {
        String temp = "";
        int min = 0;
        for (int i = 0; i < arrayNeedToSort.length; i++) {
            for (int j = i + 1; j < arrayNeedToSort.length; j++) {
                if (arrayNeedToSort[j].compareTo(arrayNeedToSort[min]) < 0) {
                    min = j;
                }
            }

            temp = arrayNeedToSort[i];
            arrayNeedToSort[i] = arrayNeedToSort[min];
            arrayNeedToSort[min] = temp;
        }

        // Printing sorted array
        for (int i = 0; i < arrayNeedToSort.length; i++) {
            System.out.print(arrayNeedToSort[i] + " ");
        }
    }
}
